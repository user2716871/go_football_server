ifeq ($(POSTGRES_SETUP),)
	POSTGRES_SETUP := user=user password=password dbname=football host=localhost port=5432 sslmode=disable
endif

INTERNAL_PKG_PATH=$(CURDIR)/internal/pkg
MIGRATION_FOLDER=$(INTERNAL_PKG_PATH)/db/migrations

.PHONY: football-migration-create
football-migration-create:
	goose -dir "$(MIGRATION_FOLDER)" create "$(name)" sql

.PHONY: football-migration-up
football-migration-up:
	goose -dir "$(MIGRATION_FOLDER)" postgres "$(POSTGRES_SETUP)" up

.PHONY: football-migration-down
football-migration-down:
	goose -dir "$(MIGRATION_FOLDER)" postgres "$(POSTGRES_SETUP)" down

.PHONY: fill-db
fill-db:
	go run tests/db/script_runner.go tests/db/fill_tables.sql

.PHONY: clean-db
clean-db:
	go run tests/db/script_runner.go tests/db/clean_tables.sql

.PHONY: build
build:
	go build -o bin/hw7 cmd/main.go

.PHONY: run
run:
	bin/hw7

.PHONY: run-unit-tests
run-unit-tests:
	go test -v -cover ./internal/server/

.PHONY: run-integration-tests
run-integration-tests:
	go test -v -cover ./internal/pkg/repository/postgresql/

.PHONY: run-in-container
run-in-container:
	docker compose up --build
