### Инструкция

- Запускаем сервис и БД в контейнерах:
```
make run-in-container
```
- Накатываем миграцию:
```
make football-migration-up
```
- Сервис готов к использованию!


### Действия

Создать миграцию:
```
make migration-create name=001_init
```

Накатить миграцию:
```
make football-migration-up
```

Откатить миграцию:
```
make football-migration-down
```

Заполнить базу тестовыми данными:
```
make fill-db
```

Очистить базу:
```
make clean-db
```

Сборка:
```
make build
```

Запуск сервера:
```
make run
```

Запустить юнит-тесты и получить степень покрытия кода ручек юнит-тестами:
```
make run-unit-tests
```

Запустить интеграционные тесты:
```
make run-integration-tests
```

### Запросы к ручкам клубов

```
curl -v -X POST "127.0.0.1:9000/clubs/create" -d '{"name": "Dinamo", "founded": 1900}'
```
```
curl -v -X GET "127.0.0.1:9000/clubs/read/?id=91"
```
```
curl -v -X PUT "127.0.0.1:9000/clubs/update/?id=91" -d '{"name": "FC Dinamo", "founded": 1950}'
```
```
curl -v -X DELETE "127.0.0.1:9000/clubs/delete/?id=91"
```
```
curl -v -X GET "127.0.0.1:9000/clubs/readall"
```

### Запросы к ручкам игроков

```
curl -v -X POST "127.0.0.1:9000/players/create" -d '{"name": "Vasya", "club_id": {"valid": true, "int32": 1}}'
```
```
curl -v -X GET "127.0.0.1:9000/players/read/?id=16"
```
```
curl -v -X PUT "127.0.0.1:9000/players/update/?id=16" -d '{"name": "=Vasya=", "club_id": {"valid": true, "int32": 2}}'
```
```
curl -v -X DELETE "127.0.0.1:9000/players/delete/?id=16"
```
```
curl -v -X GET "127.0.0.1:9000/players/readall"
```

### Запросы к ручкам пар игрок-клуб

```
curl -v -X GET "127.0.0.1:9000/pairs/read/?id=1"
```
```
curl -v -X GET "127.0.0.1:9000/pairs/readall"
```

### Ошибочные запросы

```
curl -v -X GET "127.0.0.1:9000/a/b/c/"
```
```
curl -v -X DELETE "127.0.0.1:9000/clubs/readall"
```
