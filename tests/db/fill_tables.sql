insert into clubs (name, founded)
values
    -- England --
    ('Manchester United', 1878),
    ('Liverpool', 1892),
    ('Arsenal', 1886),

    -- Spain --
    ('Barcelona', 1899),
    ('Real Madrid', 1902),

    -- Germany --
    ('Bavaria', 1900),
    ('Borussia', 1909),

    -- France --
    ('PSG', 1970),

    -- Italy --
    ('Juventus', 1897),
    ('Milan', 1861),
    ('Napoli', 1926);

insert into players (name, birthday, club_id)
values
    -- free agents --
    ('Ivanov', '2000-01-01', null),
    ('Petrov', '1990-01-01', null),
    ('Sidorov', '1980-01-01', null),

    -- MU --
    ('Rooney', '1985-10-24', 1),
    ('Ronaldo', '1985-02-05', 1),

    -- Liverpool --
    ('Salah', '1992-06-15', 2),
    ('Steven Gerrard', '1980-01-01', 2),

    -- Arsenal --
    ('Henry', '1977-08-17', 3),

    -- Barcelona --

    -- Real Madrid --
    ('Luca Modric', '1985-09-09', 5),
    ('Karim Benzema', '1985-12-19', 5),
    ('Tony Kroos', '1990-01-01', 5),

    -- Bavaria --

    -- Borussia --
    ('Haaland', '2000-01-01', 7),

    -- PSG --
    ('Messi', '1987-01-01', 8),
    ('Mbappe', '1998-01-01', 8),
    ('Neymar', '1992-01-01', 8);
