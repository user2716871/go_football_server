package main

import (
	"database/sql"
	"fmt"
	"os"
	"strings"
	"unicode"

	_ "github.com/lib/pq"
)

const (
	connStr = "user=sammy password=george23 dbname=hw5_football sslmode=disable"
)

func GetQueries(filename string) ([]string, error) {

	data, err := os.ReadFile(filename)
	if err != nil {
		return []string{}, err
	}

	dataAsString := string(data)

	queries := strings.Split(dataAsString, ";")
	for i := range queries {
		queries[i] = strings.TrimFunc(queries[i], unicode.IsSpace)
	}

	return queries, nil
}

func main() {

	if len(os.Args) != 2 {
		fmt.Println("usage: ./name script.sql")
		return
	}

	queries, err := GetQueries(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	for _, query := range queries {
		_, err := db.Exec(query)
		if err != nil {
			fmt.Println(err)
			break
		}
	}
}
