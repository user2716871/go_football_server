package server

import (
	"homework5/internal/pkg/repository"
	"net/http"
)

type GeneralServer struct {
	clubs   repository.ClubsRepo
	players repository.PlayersRepo
	pairs   repository.PlayersWithClubRepo
}

func NewGeneralServer(
	clubs repository.ClubsRepo,
	players repository.PlayersRepo,
	pairs repository.PlayersWithClubRepo,
) *GeneralServer {

	return &GeneralServer{
		clubs:   clubs,
		players: players,
		pairs:   pairs,
	}
}

func GetHandler(s *GeneralServer) func(http.ResponseWriter, *http.Request) {

	return func(res http.ResponseWriter, req *http.Request) {

		switch req.URL.Path {

		case createClubPath, dropLastChar(createClubPath):
			s.CreateClub(res, req)
		case readClubPath:
			s.ReadClub(res, req)
		case updateClubPath:
			s.UpdateClub(res, req)
		case deleteClubPath:
			s.DeleteClub(res, req)
		case readAllClubsPath, dropLastChar(readAllClubsPath):
			s.ReadAllClubs(res, req)

		case createPlayerPath, dropLastChar(createPlayerPath):
			s.CreatePlayer(res, req)
		case readPlayerPath:
			s.ReadPlayer(res, req)
		case updatePlayerPath:
			s.UpdatePlayer(res, req)
		case deletePlayerPath:
			s.DeletePlayer(res, req)
		case readAllPlayersPath, dropLastChar(readAllPlayersPath):
			s.ReadAllPlayers(res, req)

		case readPairPath:
			s.ReadPair(res, req)
		case readAllPairs, dropLastChar(readAllPairs):
			s.ReadAllPairs(res, req)

		default:
			res.Write([]byte("unsupported path\n"))
		}
	}
}
