package server

import (
	"encoding/json"
	"homework5/internal/pkg/repository"
	"net/http"
)

func (s *GeneralServer) ReadPair(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodGet {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	id, err := extractIdFromUrl(req)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	pair, err := s.pairs.Read(id)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.PlayerNotFoundError:
			res.WriteHeader(http.StatusNotFound)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, _ := json.MarshalIndent(pair, "", "  ")

	res.WriteHeader(http.StatusOK)
	res.Write(append(data, '\n'))
}

func (s *GeneralServer) ReadAllPairs(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodGet {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	if len(req.URL.Query()) != 0 {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(queryParamsDetectedErrMsg))
		return
	}

	pairs, err := s.pairs.ReadAll()

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, _ := json.MarshalIndent(pairs, "", "  ")

	res.WriteHeader(http.StatusOK)
	res.Write(append(data, '\n'))
}
