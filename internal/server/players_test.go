package server

import (
    "database/sql"
    "encoding/json"
    "github.com/stretchr/testify/assert"
    "homework5/internal/pkg/repository"
    "homework5/internal/pkg/repository/dummy"
    "io"
    "net/http"
    "net/http/httptest"
    "strings"
    "testing"
    "time"
)

func TestPlayerNotFound(t *testing.T) {

    s := NewGeneralServer(
        nil,
        &dummy.PlayersRepo{NextErr: repository.PlayerNotFoundError{}},
        nil)

    handler := GetHandler(s)

    testCases := []struct {
        method string
        path   string
        body   io.Reader
    }{
        {http.MethodPut, updatePlayerPath, strings.NewReader(somePlayer)},
        {http.MethodGet, readPlayerPath, nil},
        {http.MethodDelete, deletePlayerPath, nil},
    }

    for _, testCase := range testCases {

        req := httptest.NewRequest(
            testCase.method,
            testCase.path+idSuffix,
            testCase.body)

        w := httptest.NewRecorder()

        handler(w, req)

        resp := w.Result()

        assert.Equal(t, http.StatusNotFound, resp.StatusCode)
    }
}

func TestReadPlayerSuccess(t *testing.T) {

    player := repository.Player{
        Id:       1,
        Name:     "Ivan",
        Birthday: time.Time{},
        ClubId:   sql.NullInt32{},
    }

    s := NewGeneralServer(
        nil,
        &dummy.PlayersRepo{
            NextErr:    nil,
            OnePlayer:  player,
            AllPlayers: nil,
        },
        nil)

    handler := GetHandler(s)

    req := httptest.NewRequest(http.MethodGet, readPlayerPath+"?id=1", nil)
    w := httptest.NewRecorder()

    handler(w, req)

    resp := w.Result()

    assert.Equal(t, http.StatusOK, resp.StatusCode)

    data, _ := io.ReadAll(resp.Body)
    var receivedPlayer repository.Player
    _ = json.Unmarshal(data, &receivedPlayer)

    assert.Equal(t, player, receivedPlayer)
}

func TestReadAllPlayerSuccess(t *testing.T) {

    players := []repository.Player{
        {Id: 1, Name: "Ivanov", ClubId: sql.NullInt32{
            Int32: 1,
            Valid: true,
        }},
        {Id: 2, Name: "Petrov", ClubId: sql.NullInt32{
            Int32: 1,
            Valid: true,
        }},
        {Id: 3, Name: "Sidorov"},
    }

    s := NewGeneralServer(nil, &dummy.PlayersRepo{
        NextErr:    nil,
        AllPlayers: players,
    }, nil)

    handler := GetHandler(s)

    req := httptest.NewRequest(http.MethodGet, readAllPlayersPath, nil)
    w := httptest.NewRecorder()

    handler(w, req)

    resp := w.Result()

    assert.Equal(t, http.StatusOK, resp.StatusCode)

    data, _ := io.ReadAll(resp.Body)
    var receivedPlayers []repository.Player
    _ = json.Unmarshal(data, &receivedPlayers)

    assert.Equal(t, players, receivedPlayers)
}

func TestWritePlayerSuccess(t *testing.T) {

    s := NewGeneralServer(nil, &dummy.PlayersRepo{NextErr: nil}, nil)

    handler := GetHandler(s)

    testCases := []struct {
        method string
        path   string
        body   io.Reader
    }{
        {http.MethodPost, createPlayerPath, strings.NewReader(somePlayer)},
        {http.MethodPut, updatePlayerPath + idSuffix, strings.NewReader(somePlayer)},
        {http.MethodDelete, deletePlayerPath + idSuffix, nil},
    }

    for _, testCase := range testCases {

        req := httptest.NewRequest(
            testCase.method,
            testCase.path,
            testCase.body)

        w := httptest.NewRecorder()

        handler(w, req)

        resp := w.Result()

        assert.Equal(t, http.StatusOK, resp.StatusCode)
    }
}

func TestBadPlayerRequest(t *testing.T) {

    s := NewGeneralServer(nil, &dummy.PlayersRepo{NextErr: nil}, nil)

    handler := GetHandler(s)

    testCases := []struct {
        method string
        path   string
        body   io.Reader
        msg    string
    }{
        {
            http.MethodPost,
            createPlayerPath,
            nil,
            "no body in create request",
        },
        {
            http.MethodPost,
            createPlayerPath,
            strings.NewReader("#####"),
            "can't parse player",
        },
        {
            http.MethodPost,
            createPlayerPath + idSuffix,
            strings.NewReader(somePlayer),
            "extra query params",
        },
        {
            http.MethodGet,
            readPlayerPath,
            strings.NewReader(somePlayer),
            "no query param: 'id'",
        },
    }

    for _, testCase := range testCases {

        req := httptest.NewRequest(
            testCase.method,
            testCase.path,
            testCase.body)

        w := httptest.NewRecorder()

        handler(w, req)

        resp := w.Result()

        assert.Equal(t, http.StatusBadRequest, resp.StatusCode, testCase.msg)
    }
}
