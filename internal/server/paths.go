package server

const (
	clubsPrefix   = "/clubs"
	playersPrefix = "/players"
	pairsPrefix   = "/pairs"
)

const (
	createSuffix = "/create/"
	readSuffix   = "/read/"
	updateSuffix = "/update/"
	deleteSuffix = "/delete/"

	readAllSuffix = "/readall/"
)

const (
	createClubPath = clubsPrefix + createSuffix
	readClubPath   = clubsPrefix + readSuffix
	updateClubPath = clubsPrefix + updateSuffix
	deleteClubPath = clubsPrefix + deleteSuffix

	readAllClubsPath = clubsPrefix + readAllSuffix
)

const (
	createPlayerPath = playersPrefix + createSuffix
	readPlayerPath   = playersPrefix + readSuffix
	updatePlayerPath = playersPrefix + updateSuffix
	deletePlayerPath = playersPrefix + deleteSuffix

	readAllPlayersPath = playersPrefix + readAllSuffix
)

const (
	readPairPath = pairsPrefix + readSuffix

	readAllPairs = pairsPrefix + readAllSuffix
)

const (
	wrongMethodErrMsg         = "wrong method\n"
	queryParamsDetectedErrMsg = "query params detected\n"
)

func dropLastChar(s string) string {
	return s[:len(s)-1]
}
