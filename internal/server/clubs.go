package server

import (
	"encoding/json"
	"homework5/internal/pkg/repository"
	"io"
	"net/http"
)

func (s *GeneralServer) CreateClub(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodPost {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	if len(req.URL.Query()) != 0 {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(queryParamsDetectedErrMsg))
		return
	}

	data, err := io.ReadAll(req.Body)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	club, err := extractClub(data)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	_, err = s.clubs.Create(club)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("club created\n"))
}

func (s *GeneralServer) ReadClub(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodGet {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	id, err := extractIdFromUrl(req)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	club, err := s.clubs.Read(id)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.ClubNotFoundError:
			res.WriteHeader(http.StatusNotFound)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, _ := json.MarshalIndent(club, "", "  ")

	res.WriteHeader(http.StatusOK)
	res.Write(append(data, '\n'))
}

func (s *GeneralServer) UpdateClub(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodPut {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	id, err := extractIdFromUrl(req)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, err := io.ReadAll(req.Body)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	newValue, err := extractClub(data)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	err = s.clubs.Update(id, newValue)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.ClubNotFoundError:
			res.WriteHeader(http.StatusNotFound)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("club updated\n"))
}

func (s *GeneralServer) DeleteClub(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodDelete {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte("unsupported method\n"))
		return
	}

	id, err := extractIdFromUrl(req)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	err = s.clubs.Delete(id)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.ClubNotFoundError:
			res.WriteHeader(http.StatusNotFound)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("club deleted\n"))
}

func (s *GeneralServer) ReadAllClubs(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodGet {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	if len(req.URL.Query()) != 0 {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(queryParamsDetectedErrMsg))
		return
	}

	clubs, err := s.clubs.ReadAll()

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, _ := json.MarshalIndent(clubs, "", "  ")

	res.WriteHeader(http.StatusOK)
	res.Write(append(data, '\n'))
}
