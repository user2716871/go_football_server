package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"homework5/internal/pkg/repository"
	"math"
	"net/http"
	"strconv"
)

func extractClub(data []byte) (club repository.Club, err error) {

	err = json.Unmarshal(data, &club)
	if err != nil {
		return repository.Club{}, err
	}

	if club.Name == "" {
		return repository.Club{}, errors.New("club with empty name")
	}

	return club, nil
}

func extractPlayer(data []byte) (player repository.Player, err error) {

	err = json.Unmarshal(data, &player)
	if err != nil {
		return repository.Player{}, err
	}

	if player.Name == "" {
		return repository.Player{}, errors.New("player with empty name")
	}

	return player, nil
}

func extractIdFromUrl(req *http.Request) (int, error) {

	idStr := req.URL.Query().Get("id")
	if idStr == "" {
		return 0, errors.New("no 'id' query param")
	}

	id, err := strconv.Atoi(idStr)
	if err != nil {
		return 0, fmt.Errorf("can't parse id form url: %w", err)
	}

	if !(1 <= id && id <= math.MaxInt32) {
		return 0, errors.New("id is out of bounds")
	}

	return id, nil
}
