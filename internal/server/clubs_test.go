package server

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"homework5/internal/pkg/repository"
	"homework5/internal/pkg/repository/dummy"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestClubNotFound(t *testing.T) {

	s := NewGeneralServer(
		&dummy.ClubsRepo{NextErr: repository.ClubNotFoundError{}},
		&dummy.PlayersRepo{},
		&dummy.PlayersWithClubRepo{})

	handler := GetHandler(s)

	testCases := []struct {
		method string
		path   string
		body   io.Reader
	}{
		{http.MethodGet, readClubPath, nil},
		{http.MethodPut, updateClubPath, strings.NewReader(someClub)},
		{http.MethodDelete, deleteClubPath, nil},
	}

	for _, testCase := range testCases {

		req := httptest.NewRequest(
			testCase.method,
			testCase.path+idSuffix,
			testCase.body)

		w := httptest.NewRecorder()

		handler(w, req)

		resp := w.Result()

		assert.Equal(t, http.StatusNotFound, resp.StatusCode)
	}
}

func TestReadClubSuccess(t *testing.T) {

	club := repository.Club{
		Id:      1,
		Name:    "Club1",
		Founded: 1900,
	}

	s := NewGeneralServer(&dummy.ClubsRepo{
		NextErr:  nil,
		OneClub:  club,
		AllClubs: nil,
	}, nil, nil)

	handler := GetHandler(s)

	req := httptest.NewRequest(http.MethodGet, readClubPath+"?id=1", nil)
	w := httptest.NewRecorder()

	handler(w, req)

	resp := w.Result()

	assert.Equal(t, http.StatusOK, resp.StatusCode)

	data, _ := io.ReadAll(resp.Body)
	var receivedClub repository.Club
	_ = json.Unmarshal(data, &receivedClub)

	assert.Equal(t, club, receivedClub)
}

func TestReadAllClubSuccess(t *testing.T) {

	clubs := []repository.Club{
		{Id: 1, Name: "Club1", Founded: 1800},
		{Id: 2, Name: "Club2", Founded: 1900},
		{Id: 3, Name: "Club3", Founded: 2000},
	}

	s := NewGeneralServer(&dummy.ClubsRepo{
		NextErr:  nil,
		AllClubs: clubs,
	}, nil, nil)

	handler := GetHandler(s)

	req := httptest.NewRequest(http.MethodGet, readAllClubsPath, nil)
	w := httptest.NewRecorder()

	handler(w, req)

	resp := w.Result()

	assert.Equal(t, http.StatusOK, resp.StatusCode)

	data, _ := io.ReadAll(resp.Body)
	var receivedClubs []repository.Club
	_ = json.Unmarshal(data, &receivedClubs)

	assert.Equal(t, clubs, receivedClubs)
}

func TestWriteClubSuccess(t *testing.T) {

	s := NewGeneralServer(&dummy.ClubsRepo{NextErr: nil}, nil, nil)

	handler := GetHandler(s)

	testCases := []struct {
		method string
		path   string
		body   io.Reader
	}{
		{http.MethodPut, updateClubPath + idSuffix, strings.NewReader(someClub)},
		{http.MethodPost, createClubPath, strings.NewReader(someClub)},
		{http.MethodDelete, deleteClubPath + idSuffix, nil},
	}

	for _, testCase := range testCases {

		req := httptest.NewRequest(
			testCase.method,
			testCase.path,
			testCase.body)

		w := httptest.NewRecorder()

		handler(w, req)

		resp := w.Result()

		assert.Equal(t, http.StatusOK, resp.StatusCode)
	}
}

func TestBadClubRequest(t *testing.T) {

	s := NewGeneralServer(&dummy.ClubsRepo{NextErr: nil}, nil, nil)

	handler := GetHandler(s)

	testCases := []struct {
		method string
		path   string
		body   io.Reader
		msg    string
	}{
		{
			http.MethodPost,
			createClubPath,
			nil,
			"no body in create request",
		},
		{
			http.MethodPost,
			createClubPath,
			strings.NewReader("#####"),
			"can't parse club",
		},
		{
			http.MethodPost,
			createClubPath + idSuffix,
			strings.NewReader(someClub),
			"extra query params",
		},
		{
			http.MethodGet,
			readClubPath,
			strings.NewReader(someClub),
			"no query param: 'id'",
		},
	}

	for _, testCase := range testCases {

		req := httptest.NewRequest(
			testCase.method,
			testCase.path,
			testCase.body)

		w := httptest.NewRecorder()

		handler(w, req)

		resp := w.Result()

		assert.Equal(t, http.StatusBadRequest, resp.StatusCode, testCase.msg)
	}
}
