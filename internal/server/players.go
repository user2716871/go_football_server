package server

import (
	"encoding/json"
	"homework5/internal/pkg/repository"
	"io"
	"net/http"
)

func (s *GeneralServer) CreatePlayer(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodPost {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	if len(req.URL.Query()) != 0 {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(queryParamsDetectedErrMsg))
		return
	}

	data, err := io.ReadAll(req.Body)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	player, err := extractPlayer(data)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	_, err = s.players.Create(player)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.ClubNotFoundError:
			res.WriteHeader(http.StatusBadRequest)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("player created\n"))
}

func (s *GeneralServer) ReadPlayer(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodGet {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	id, err := extractIdFromUrl(req)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	player, err := s.players.Read(id)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.PlayerNotFoundError:
			res.WriteHeader(http.StatusNotFound)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, _ := json.MarshalIndent(player, "", "  ")

	res.WriteHeader(http.StatusOK)
	res.Write(append(data, '\n'))
}

func (s *GeneralServer) UpdatePlayer(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodPut {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	id, err := extractIdFromUrl(req)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, err := io.ReadAll(req.Body)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	newValue, err := extractPlayer(data)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	err = s.players.Update(id, newValue)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.PlayerNotFoundError:
			res.WriteHeader(http.StatusNotFound)
		case repository.ClubNotFoundError:
			res.WriteHeader(http.StatusBadRequest)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("player updated\n"))
}

func (s *GeneralServer) DeletePlayer(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodDelete {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	id, err := extractIdFromUrl(req)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	err = s.players.Delete(id)

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		case repository.PlayerNotFoundError:
			res.WriteHeader(http.StatusNotFound)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("player deleted\n"))
}

func (s *GeneralServer) ReadAllPlayers(res http.ResponseWriter, req *http.Request) {

	if req.Method != http.MethodGet {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(wrongMethodErrMsg))
		return
	}

	if len(req.URL.Query()) != 0 {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(queryParamsDetectedErrMsg))
		return
	}

	players, err := s.players.ReadAll()

	if err != nil {
		switch err.(type) {
		case repository.DBError:
			res.WriteHeader(http.StatusInternalServerError)
		default:
			res.WriteHeader(http.StatusInternalServerError)
		}
		res.Write([]byte(err.Error() + "\n"))
		return
	}

	data, _ := json.MarshalIndent(players, "", "  ")

	res.WriteHeader(http.StatusOK)
	res.Write(append(data, '\n'))
}
