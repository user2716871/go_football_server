package server

import (
	"database/sql"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"homework5/internal/pkg/repository"
	"homework5/internal/pkg/repository/dummy"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPairNotFound(t *testing.T) {

	s := NewGeneralServer(
		nil,
		nil,
		&dummy.PlayersWithClubRepo{NextErr: repository.PlayerNotFoundError{}})

	handler := GetHandler(s)

	req := httptest.NewRequest(
		http.MethodGet,
		readPairPath+idSuffix,
		nil)

	w := httptest.NewRecorder()

	handler(w, req)

	resp := w.Result()

	assert.Equal(t, http.StatusNotFound, resp.StatusCode)
}

func TestReadPairSuccess(t *testing.T) {

	pair := repository.PlayerWithClub{
		PlayerId:   1,
		PlayerName: "Ivanov",
		ClubId: sql.NullInt32{
			Int32: 1,
			Valid: true,
		},
		ClubName: sql.NullString{
			String: "Barcelona",
			Valid:  true,
		},
	}

	s := NewGeneralServer(nil, nil, &dummy.PlayersWithClubRepo{
		NextErr:  nil,
		OnePair:  pair,
		AllPairs: nil,
	})

	handler := GetHandler(s)

	req := httptest.NewRequest(http.MethodGet, readPairPath+"?id=1", nil)
	w := httptest.NewRecorder()

	handler(w, req)

	resp := w.Result()

	assert.Equal(t, http.StatusOK, resp.StatusCode)

	data, _ := io.ReadAll(resp.Body)
	var receivedPair repository.PlayerWithClub
	_ = json.Unmarshal(data, &receivedPair)

	assert.Equal(t, pair, receivedPair)
}

func TestBadPairRequest(t *testing.T) {

	s := NewGeneralServer(nil, nil, &dummy.PlayersWithClubRepo{NextErr: nil})

	handler := GetHandler(s)

	testCases := []struct {
		method string
		path   string
		body   io.Reader
		msg    string
	}{
		{
			http.MethodGet,
			readPairPath + "?id=0",
			nil,
			"id out of range",
		},
		{
			http.MethodGet,
			readPairPath,
			nil,
			"no query param: 'id'",
		},
		{
			http.MethodGet,
			readPairPath + "?id=qwerty",
			nil,
			"can't parse id",
		},
		{
			http.MethodGet,
			readAllPairs + idSuffix,
			nil,
			"extra query params",
		},
	}

	for _, testCase := range testCases {

		req := httptest.NewRequest(
			testCase.method,
			testCase.path,
			testCase.body)

		w := httptest.NewRecorder()

		handler(w, req)

		resp := w.Result()

		assert.Equal(t, http.StatusBadRequest, resp.StatusCode, testCase.msg)
	}
}
