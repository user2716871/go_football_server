package server

import (
	"github.com/stretchr/testify/assert"
	"homework5/internal/pkg/repository"
	"homework5/internal/pkg/repository/dummy"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

var methods = []string{
	http.MethodPost,
	http.MethodGet,
	http.MethodPut,
	http.MethodDelete,
}

const idSuffix = "?id=123"
const someClub = `{"id": 1, "name": "Club1", "founded": 1900}`
const somePlayer = `{"id": 1, "name": "Player1", "birthday": "2000-01-01T00:00:00Z"}`

func TestMethodForReading(t *testing.T) {

	readPaths := []string{
		readClubPath + idSuffix,
		readPlayerPath + idSuffix,
		readPairPath + idSuffix,

		readAllPairs,
		readAllClubsPath,
		readAllPlayersPath,
	}

	s := NewGeneralServer(
		&dummy.ClubsRepo{NextErr: nil},
		&dummy.PlayersRepo{NextErr: nil},
		&dummy.PlayersWithClubRepo{NextErr: nil})

	handler := GetHandler(s)

	for _, path := range readPaths {
		for _, method := range methods {

			req := httptest.NewRequest(method, path, nil)
			w := httptest.NewRecorder()

			handler(w, req)

			resp := w.Result()
			// body, _ := io.ReadAll(resp.Body)

			var expected int
			if method == http.MethodGet {
				expected = http.StatusOK
			} else {
				expected = http.StatusBadRequest
			}

			assert.Equal(t, resp.StatusCode, expected, "%s %s", method, path)
		}
	}
}

func TestMethodForDeleting(t *testing.T) {

	deletePaths := []string{
		deleteClubPath + idSuffix,
		deletePlayerPath + idSuffix,
	}

	s := NewGeneralServer(
		&dummy.ClubsRepo{NextErr: nil},
		&dummy.PlayersRepo{NextErr: nil},
		&dummy.PlayersWithClubRepo{NextErr: nil})

	handler := GetHandler(s)

	for _, path := range deletePaths {
		for _, method := range methods {

			req := httptest.NewRequest(method, path, nil)
			w := httptest.NewRecorder()

			handler(w, req)

			resp := w.Result()

			var expected int
			if method == http.MethodDelete {
				expected = http.StatusOK
			} else {
				expected = http.StatusBadRequest
			}

			assert.Equal(t, resp.StatusCode, expected, "%s %s", method, path)
		}
	}
}

func TestMethodForUpdating(t *testing.T) {

	s := NewGeneralServer(
		&dummy.ClubsRepo{NextErr: nil},
		&dummy.PlayersRepo{NextErr: nil},
		&dummy.PlayersWithClubRepo{NextErr: nil})

	handler := GetHandler(s)

	testCases := []struct {
		path string
		body string
	}{
		{updateClubPath + idSuffix, someClub},
		{updatePlayerPath + idSuffix, somePlayer},
	}

	for _, testCase := range testCases {
		for _, method := range methods {

			req := httptest.NewRequest(method, testCase.path, strings.NewReader(testCase.body))
			w := httptest.NewRecorder()

			handler(w, req)

			resp := w.Result()

			var expected int
			if method == http.MethodPut {
				expected = http.StatusOK
			} else {
				expected = http.StatusBadRequest
			}

			assert.Equal(t, resp.StatusCode, expected)
		}
	}
}

func TestMethodForCreation(t *testing.T) {

	s := NewGeneralServer(
		&dummy.ClubsRepo{NextErr: nil},
		&dummy.PlayersRepo{NextErr: nil},
		&dummy.PlayersWithClubRepo{NextErr: nil})

	handler := GetHandler(s)

	testCases := []struct {
		path string
		body string
	}{
		{createClubPath, someClub},
		{createPlayerPath, somePlayer},
	}

	for _, testCase := range testCases {
		for _, method := range methods {

			req := httptest.NewRequest(method, testCase.path, strings.NewReader(testCase.body))
			w := httptest.NewRecorder()

			handler(w, req)

			resp := w.Result()

			var expected int
			if method == http.MethodPost {
				expected = http.StatusOK
			} else {
				expected = http.StatusBadRequest
			}

			assert.Equal(t, resp.StatusCode, expected)
		}
	}
}

func TestDataBaseErrors(t *testing.T) {

	s := NewGeneralServer(
		&dummy.ClubsRepo{NextErr: repository.DBError{}},
		&dummy.PlayersRepo{NextErr: repository.DBError{}},
		&dummy.PlayersWithClubRepo{NextErr: repository.DBError{}})

	handler := GetHandler(s)

	testCases := []struct {
		method string
		path   string
		body   io.Reader
	}{
		{http.MethodGet, readClubPath + idSuffix, nil},
		{http.MethodGet, readAllClubsPath, nil},

		{http.MethodGet, readPlayerPath + idSuffix, nil},
		{http.MethodGet, readAllPlayersPath, nil},

		{http.MethodGet, readPairPath + idSuffix, nil},
		{http.MethodGet, readAllPairs, nil},

		{http.MethodDelete, deleteClubPath + idSuffix, nil},
		{http.MethodDelete, deletePlayerPath + idSuffix, nil},

		{http.MethodPost, createClubPath, strings.NewReader(someClub)},
		{http.MethodPost, createPlayerPath, strings.NewReader(somePlayer)},
	}

	for _, testCase := range testCases {
		req := httptest.NewRequest(testCase.method, testCase.path, testCase.body)
		w := httptest.NewRecorder()

		handler(w, req)

		resp := w.Result()
		assert.Equal(t, http.StatusInternalServerError, resp.StatusCode, "%v", testCase)
	}
}
