package repository

import "strconv"

type DBError struct {
	Msg string
}

func (err DBError) Error() string {
	return err.Msg
}

type ClubNotFoundError struct {
	ClubId int
}

func (err ClubNotFoundError) Error() string {
	return "no club with id = " + strconv.Itoa(err.ClubId)
}

type PlayerNotFoundError struct {
	PlayerId int
}

func (err PlayerNotFoundError) Error() string {
	return "no player with id = " + strconv.Itoa(err.PlayerId)
}
