package repository

type ClubsRepo interface {
	Create(club Club) (int, error)
	Read(id int) (Club, error)
	Update(id int, newValue Club) error
	Delete(id int) error

	ReadAll() ([]Club, error)
}

type PlayersRepo interface {
	Create(club Player) (int, error)
	Read(id int) (Player, error)
	Update(id int, newValue Player) error
	Delete(id int) error

	ReadAll() ([]Player, error)
}

type PlayersWithClubRepo interface {
	Read(id int) (PlayerWithClub, error)

	ReadAll() ([]PlayerWithClub, error)
}
