package postgresql

import (
	"database/sql"
	"github.com/stretchr/testify/assert"
	"homework5/internal/pkg/repository"
	"testing"
	"time"
)

func TestCreatePlayerWithClubSuccess(t *testing.T) {

	var err error

	clubs := NewClubsRepo()
	players := NewPlayersRepo()

	club := repository.Club{
		Id:      -1,
		Name:    "FC Rostov",
		Founded: 2000,
	}

	club.Id, err = clubs.Create(club)
	assert.Equal(t, err, nil)

	sentPlayer := repository.Player{
		Id:       -1,
		Name:     "Ivan Ivanov",
		Birthday: time.Time{},
		ClubId:   sql.NullInt32{Valid: true, Int32: int32(club.Id)},
	}

	sentPlayer.Id, err = players.Create(sentPlayer)
	assert.Equal(t, err, nil)

	receivedPlayer, err := players.Read(sentPlayer.Id)
	assert.Equal(t, err, nil)

	sentPlayer.Birthday = receivedPlayer.Birthday
	assert.Equal(t, sentPlayer, receivedPlayer)
}

func TestUpdatePlayerWithClubSuccess(t *testing.T) {

	var err error

	clubs := NewClubsRepo()
	players := NewPlayersRepo()

	club := repository.Club{
		Id:      -1,
		Name:    "FC Rostov",
		Founded: 2000,
	}

	player := repository.Player{
		Id:       -1,
		Name:     "Ivan Ivanov",
		Birthday: time.Time{},
		ClubId:   sql.NullInt32{},
	}

	club.Id, err = clubs.Create(club)
	assert.Equal(t, err, nil)

	player.Id, err = players.Create(player)
	assert.Equal(t, err, nil)

	player.ClubId = sql.NullInt32{Valid: true, Int32: int32(club.Id)}
	err = players.Update(player.Id, player)
	assert.Equal(t, err, nil)

	expected := player
	actual, err := players.Read(expected.Id)
	assert.Equal(t, err, nil)

	expected.Birthday = actual.Birthday
	assert.Equal(t, expected, actual)
}

func TestDeleteClubWithPlayersSuccess(t *testing.T) {

	var err error

	clubs := NewClubsRepo()
	players := NewPlayersRepo()

	club := repository.Club{
		Id:      -1,
		Name:    "FC Rostov",
		Founded: 2000,
	}

	club.Id, err = clubs.Create(club)
	assert.Equal(t, err, nil)

	player := repository.Player{
		Id:       -1,
		Name:     "Ivan Ivanov",
		Birthday: time.Time{},
		ClubId:   sql.NullInt32{Valid: true, Int32: int32(club.Id)},
	}

	player.Id, err = players.Create(player)
	assert.Equal(t, err, nil)

	err = clubs.Delete(club.Id)
	assert.Equal(t, err, nil)

	player, err = players.Read(player.Id)
	assert.Equal(t, err, nil)

	assert.Equal(t, player.ClubId, sql.NullInt32{
		Int32: 0,
		Valid: false,
	})
}

func TestClubOfPlayerNotFound(t *testing.T) {

	var err error

	clubs := NewClubsRepo()
	players := NewPlayersRepo()

	club := repository.Club{
		Id:      -1,
		Name:    "FC Rostov",
		Founded: 2000,
	}

	club.Id, err = clubs.Create(club)
	assert.Equal(t, err, nil)

	player := repository.Player{
		Id:       -1,
		Name:     "Ivan Ivanov",
		Birthday: time.Time{},
		ClubId:   sql.NullInt32{Valid: true, Int32: int32(club.Id)},
	}

	player.Id, err = players.Create(player)
	assert.Equal(t, err, nil)

	strangeClubId := sql.NullInt32{
		Valid: true,
		Int32: 0,
	}

	// can't create
	_, err = players.Create(repository.Player{ClubId: strangeClubId})
	_, ok := err.(repository.ClubNotFoundError)
	assert.True(t, ok)

	// can't update
	player.ClubId = strangeClubId
	err = players.Update(player.Id, player)
	_, ok = err.(repository.ClubNotFoundError)
	assert.True(t, ok)
}
