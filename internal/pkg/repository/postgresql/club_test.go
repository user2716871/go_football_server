package postgresql

import (
	"github.com/stretchr/testify/assert"
	"homework5/internal/pkg/repository"
	"testing"

	_ "github.com/lib/pq"
)

func TestCreateAndReadClubSuccess(t *testing.T) {

	repo := NewClubsRepo()
	sentClub := repository.Club{
		Id:      -1,
		Name:    "Spartak",
		Founded: 1920,
	}

	id, err := repo.Create(sentClub)
	assert.Equal(t, err, nil)

	sentClub.Id = id

	receivedClub, err := repo.Read(id)
	assert.Equal(t, err, nil)
	assert.Equal(t, sentClub, receivedClub)
}

func TestDeleteClubSuccess(t *testing.T) {

	repo := NewClubsRepo()
	club := repository.Club{
		Id:      -1,
		Name:    "MU",
		Founded: 1850,
	}

	id, err := repo.Create(club)
	assert.Equal(t, err, nil)

	err = repo.Delete(id)
	assert.Equal(t, err, nil)

	err = repo.Delete(id)
	_, ok := err.(repository.ClubNotFoundError)
	assert.True(t, ok)
}

func TestUpdateClubSuccess(t *testing.T) {

	repo := NewClubsRepo()
	oldValue := repository.Club{Id: -1, Name: "MU", Founded: 1850}
	newValue := repository.Club{Id: -1, Name: "Manchester United", Founded: 1900}

	id, err := repo.Create(oldValue)
	assert.Equal(t, err, nil)

	err = repo.Update(id, newValue)
	assert.Equal(t, err, nil)

	updatedValue, err := repo.Read(id)
	assert.Equal(t, err, nil)

	newValue.Id = id
	assert.Equal(t, newValue, updatedValue)
}

func TestClubNotFound(t *testing.T) {

	strangeId := 0
	repo := NewClubsRepo()

	_, err := repo.Read(strangeId)
	_, ok := err.(repository.ClubNotFoundError)
	assert.True(t, ok)

	err = repo.Update(strangeId, repository.Club{})
	_, ok = err.(repository.ClubNotFoundError)
	assert.True(t, ok)

	err = repo.Delete(strangeId)
	_, ok = err.(repository.ClubNotFoundError)
	assert.True(t, ok)
}
