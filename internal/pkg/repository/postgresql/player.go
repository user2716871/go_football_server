package postgresql

import (
	"homework5/internal/pkg/repository"
)

type PlayersRepo struct{}

func NewPlayersRepo() *PlayersRepo {
	return new(PlayersRepo)
}

const createPlayerQuery = `
    insert into players (name, birthday, club_id)
    values
        ($1, $2, $3)
    returning id
`

func (repo *PlayersRepo) Create(player repository.Player) (int, error) {

	db, err := GetDB()
	if err != nil {
		return 0, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	if player.ClubId.Valid {
		rows, err := db.Query(readClubQuery, player.ClubId.Int32)
		if err != nil {
			return 0, repository.DBError{Msg: err.Error()}
		}
		defer rows.Close()

		clubExists := rows.Next()
		if !clubExists {
			return 0, repository.ClubNotFoundError{ClubId: int(player.ClubId.Int32)}
		}
	}

	res := db.QueryRow(createPlayerQuery, player.Name, player.Birthday, player.ClubId)
	var newRecordId int
	err = res.Scan(&newRecordId)
	if err != nil {
		return 0, repository.DBError{Msg: err.Error()}
	}

	return newRecordId, nil
}

const readPlayerQuery = `
    select id, name, birthday, club_id from players
    where id = $1
`

func (repo *PlayersRepo) Read(id int) (repository.Player, error) {

	db, err := GetDB()
	if err != nil {
		return repository.Player{}, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	rows, err := db.Query(readPlayerQuery, id)
	if err != nil {
		return repository.Player{}, repository.DBError{Msg: err.Error()}
	}
	defer rows.Close()

	playerExists := rows.Next()
	if !playerExists {
		return repository.Player{}, repository.PlayerNotFoundError{PlayerId: id}
	}

	var result repository.Player
	err = rows.Scan(&result.Id, &result.Name, &result.Birthday, &result.ClubId)
	if err != nil {
		return repository.Player{}, repository.DBError{Msg: err.Error()}
	}
	return result, nil
}

const updatePlayerQuery = `
    update players 
    set 
        name = $2,
        birthday = $3,
        club_id = $4
    where id = $1
`

func (repo *PlayersRepo) Update(id int, newValue repository.Player) error {

	db, err := GetDB()
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	rows, err := db.Query(readPlayerQuery, id)
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}
	defer rows.Close()

	playerExists := rows.Next()
	if !playerExists {
		return repository.PlayerNotFoundError{PlayerId: id}
	}

	if newValue.ClubId.Valid {
		rows, err := db.Query(readClubQuery, newValue.ClubId.Int32)
		if err != nil {
			return repository.DBError{Msg: err.Error()}
		}
		defer rows.Close()

		clubExists := rows.Next()
		if !clubExists {
			return repository.ClubNotFoundError{ClubId: int(newValue.ClubId.Int32)}
		}
	}

	_, err = db.Exec(
		updatePlayerQuery,
		id,
		newValue.Name,
		newValue.Birthday,
		newValue.ClubId)

	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}

	return nil
}

const deletePlayerQuery = `
    delete from players
    where id = $1
`

func (repo *PlayersRepo) Delete(id int) error {

	db, err := GetDB()
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	result, err := db.Exec(deletePlayerQuery, id)
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}

	if rowsCount, _ := result.RowsAffected(); rowsCount == 0 {
		return repository.PlayerNotFoundError{PlayerId: id}
	}

	return nil
}

func (repo *PlayersRepo) ReadAll() ([]repository.Player, error) {

	db, err := GetDB()
	if err != nil {
		return []repository.Player{}, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	rows, err := db.Query("select id, name, birthday, club_id from players order by id")
	if err != nil {
		return []repository.Player{}, repository.DBError{Msg: err.Error()}
	}
	defer rows.Close()

	players := make([]repository.Player, 0)

	for rows.Next() {
		var player repository.Player
		err = rows.Scan(&player.Id, &player.Name, &player.Birthday, &player.ClubId)
		if err != nil {
			return []repository.Player{}, repository.DBError{Msg: err.Error()}
		}
		players = append(players, player)
	}

	return players, nil
}
