package postgresql

import (
	"homework5/internal/pkg/repository"
)

type ClubsRepo struct{}

func NewClubsRepo() *ClubsRepo {
	return new(ClubsRepo)
}

const createClubQuery = `
    insert into clubs (name, founded)
    values
        ($1, $2)
    returning id
`

func (repo *ClubsRepo) Create(club repository.Club) (int, error) {

	db, err := GetDB()
	if err != nil {
		return 0, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	res := db.QueryRow(createClubQuery, club.Name, club.Founded)
	var newRecordId int
	err = res.Scan(&newRecordId)
	if err != nil {
		return 0, repository.DBError{Msg: err.Error()}
	}

	return newRecordId, nil
}

const readClubQuery = `
    select id, name, founded from clubs
    where id = $1
`

func (repo *ClubsRepo) Read(id int) (repository.Club, error) {

	db, err := GetDB()
	if err != nil {
		return repository.Club{}, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	rows, err := db.Query(readClubQuery, id)
	if err != nil {
		return repository.Club{}, repository.DBError{Msg: err.Error()}
	}
	defer rows.Close()

	clubExists := rows.Next()
	if !clubExists {
		return repository.Club{}, repository.ClubNotFoundError{ClubId: id}
	}

	var result repository.Club
	err = rows.Scan(&result.Id, &result.Name, &result.Founded)
	if err != nil {
		return repository.Club{}, repository.DBError{Msg: err.Error()}
	}

	return result, nil
}

const updateClubQuery = `
    update clubs
    set name = $2, founded = $3
    where id = $1
`

func (repo *ClubsRepo) Update(id int, newValue repository.Club) error {

	db, err := GetDB()
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	result, err := db.Exec(updateClubQuery, id, newValue.Name, newValue.Founded)
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}

	if rowsCount, _ := result.RowsAffected(); rowsCount == 0 {
		return repository.ClubNotFoundError{ClubId: id}
	}

	return nil
}

const deleteClubQuery = `
    delete from clubs
    where id = $1
`

func (repo *ClubsRepo) Delete(id int) error {

	db, err := GetDB()
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	result, err := db.Exec(deleteClubQuery, id)
	if err != nil {
		return repository.DBError{Msg: err.Error()}
	}

	if rowsCount, _ := result.RowsAffected(); rowsCount == 0 {
		return repository.ClubNotFoundError{ClubId: id}
	}

	db.Exec("update players set club_id = null where club_id = $1", id)
	return nil
}

func (repo *ClubsRepo) ReadAll() ([]repository.Club, error) {

	db, err := GetDB()
	if err != nil {
		return []repository.Club{}, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	rows, err := db.Query("select id, name, founded from clubs order by id")
	if err != nil {
		return []repository.Club{}, repository.DBError{Msg: err.Error()}
	}
	defer rows.Close()

	clubs := make([]repository.Club, 0)

	for rows.Next() {
		var club repository.Club
		err = rows.Scan(&club.Id, &club.Name, &club.Founded)
		if err != nil {
			return []repository.Club{}, repository.DBError{Msg: err.Error()}
		}
		clubs = append(clubs, club)
	}

	return clubs, nil
}
