package postgresql

import (
	"database/sql"
	"fmt"
)

const (
	user     = "user"
	password = "password"
	host     = "postgresql-football"
	port     = 5432
	dbName   = "football"
)

var connStr = fmt.Sprintf(
	"postgres://%s:%s@%s:%d/%s?sslmode=disable",
	user, password, host, port, dbName)

func GetDB() (*sql.DB, error) {
	return sql.Open("postgres", connStr)
}
