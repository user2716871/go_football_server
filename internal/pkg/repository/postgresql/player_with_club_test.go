package postgresql

import (
	"database/sql"
	"github.com/stretchr/testify/assert"
	"homework5/internal/pkg/repository"
	"testing"
	"time"
)

func TestPlayerWithClubReadSuccess(t *testing.T) {

	clubs := NewClubsRepo()
	players := NewPlayersRepo()
	pairs := NewPlayersWithClubRepo()

	club := repository.Club{
		Id:      -1,
		Name:    "Barcelona",
		Founded: 1900,
	}

	clubId, err := clubs.Create(club)
	assert.Equal(t, err, nil)

	player := repository.Player{
		Id:       -1,
		Name:     "Messi",
		Birthday: time.Time{},
		ClubId: sql.NullInt32{
			Valid: true,
			Int32: int32(clubId),
		},
	}

	playerId, err := players.Create(player)
	assert.Equal(t, err, nil)

	expectedPair := repository.PlayerWithClub{
		PlayerId:   playerId,
		PlayerName: player.Name,
		ClubId: sql.NullInt32{
			Valid: true,
			Int32: int32(clubId),
		},
		ClubName: sql.NullString{
			Valid:  true,
			String: club.Name,
		},
	}

	actualPair, err := pairs.Read(playerId)
	assert.Equal(t, expectedPair, actualPair)
}
