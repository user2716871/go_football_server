package postgresql

import (
	"database/sql"
	"github.com/stretchr/testify/assert"
	"homework5/internal/pkg/repository"
	"testing"
	"time"
)

func TestCreateAndReadPlayerSuccess(t *testing.T) {

	repo := NewPlayersRepo()
	sentClub := repository.Player{
		Id:       -1,
		Name:     "Ivanov",
		Birthday: time.Time{},
		ClubId:   sql.NullInt32{},
	}

	id, err := repo.Create(sentClub)
	assert.Equal(t, err, nil)

	receivedClub, err := repo.Read(id)

	sentClub.Id = id
	sentClub.Birthday = receivedClub.Birthday // different locals, not important

	assert.Equal(t, err, nil)
	assert.Equal(t, sentClub, receivedClub)
}

func TestDeletePlayerSuccess(t *testing.T) {

	repo := NewPlayersRepo()
	player := repository.Player{
		Id:       -1,
		Name:     "Petrov",
		Birthday: time.Time{},
		ClubId:   sql.NullInt32{},
	}

	id, err := repo.Create(player)
	assert.Equal(t, err, nil)

	err = repo.Delete(id)
	assert.Equal(t, err, nil)

	err = repo.Delete(id)
	_, ok := err.(repository.PlayerNotFoundError)
	assert.True(t, ok)
}

func TestPlayerNotFound(t *testing.T) {

	strangeId := 0
	repo := NewPlayersRepo()

	_, err := repo.Read(strangeId)
	_, ok := err.(repository.PlayerNotFoundError)
	assert.True(t, ok)

	err = repo.Update(strangeId, repository.Player{
		ClubId: sql.NullInt32{Valid: true, Int32: int32(strangeId)},
	})
	_, ok = err.(repository.PlayerNotFoundError)
	assert.True(t, ok)

	err = repo.Delete(strangeId)
	_, ok = err.(repository.PlayerNotFoundError)
	assert.True(t, ok)
}
