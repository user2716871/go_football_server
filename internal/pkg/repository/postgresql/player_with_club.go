package postgresql

import (
	"homework5/internal/pkg/repository"
)

type PlayersWithClubRepo struct{}

func NewPlayersWithClubRepo() *PlayersWithClubRepo {
	return new(PlayersWithClubRepo)
}

func (repo *PlayersWithClubRepo) Read(id int) (repository.PlayerWithClub, error) {

	const joinQuery = `
		select p.id, p.name, c.id, c.name
		from players p left join clubs c
		on p.club_id = c.id
		where p.id = $1
`
	db, err := GetDB()
	if err != nil {
		return repository.PlayerWithClub{}, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	rows, err := db.Query(joinQuery, id)
	if err != nil {
		return repository.PlayerWithClub{}, repository.DBError{Msg: err.Error()}
	}
	defer rows.Close()

	playerExists := rows.Next()
	if !playerExists {
		return repository.PlayerWithClub{}, repository.PlayerNotFoundError{PlayerId: id}
	}

	var pair repository.PlayerWithClub
	err = rows.Scan(&pair.PlayerId, &pair.PlayerName, &pair.ClubId, &pair.ClubName)
	if err != nil {
		return repository.PlayerWithClub{}, repository.DBError{Msg: err.Error()}
	}

	return pair, nil
}

func (repo *PlayersWithClubRepo) ReadAll() ([]repository.PlayerWithClub, error) {

	const joinQuery = `
		select p.id, p.name, c.id, c.name
		from players p left join clubs c
		on p.club_id = c.id
		order by p.id
`

	db, err := GetDB()
	if err != nil {
		return []repository.PlayerWithClub{}, repository.DBError{Msg: err.Error()}
	}
	defer db.Close()

	rows, err := db.Query(joinQuery)
	if err != nil {
		return []repository.PlayerWithClub{}, repository.DBError{Msg: err.Error()}
	}
	defer rows.Close()

	pairs := make([]repository.PlayerWithClub, 0)
	for rows.Next() {
		var pair repository.PlayerWithClub
		err = rows.Scan(&pair.PlayerId, &pair.PlayerName, &pair.ClubId, &pair.ClubName)
		if err != nil {
			return []repository.PlayerWithClub{}, repository.DBError{Msg: err.Error()}
		}
		pairs = append(pairs, pair)
	}

	return pairs, nil
}
