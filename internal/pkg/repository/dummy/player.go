package dummy

import "homework5/internal/pkg/repository"

type PlayersRepo struct {
	NextErr    error
	OnePlayer  repository.Player
	AllPlayers []repository.Player
}

func (repo *PlayersRepo) Create(player repository.Player) (int, error) {
	return 1, repo.NextErr
}

func (repo *PlayersRepo) Read(id int) (repository.Player, error) {
	return repo.OnePlayer, repo.NextErr
}

func (repo *PlayersRepo) Update(id int, newValue repository.Player) error {
	return repo.NextErr
}

func (repo *PlayersRepo) Delete(id int) error {
	return repo.NextErr
}

func (repo *PlayersRepo) ReadAll() ([]repository.Player, error) {
	return repo.AllPlayers, repo.NextErr
}
