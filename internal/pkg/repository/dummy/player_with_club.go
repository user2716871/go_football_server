package dummy

import "homework5/internal/pkg/repository"

type PlayersWithClubRepo struct {
	NextErr  error
	OnePair  repository.PlayerWithClub
	AllPairs []repository.PlayerWithClub
}

func (repo *PlayersWithClubRepo) Read(id int) (repository.PlayerWithClub, error) {
	return repo.OnePair, repo.NextErr
}

func (repo *PlayersWithClubRepo) ReadAll() ([]repository.PlayerWithClub, error) {
	return repo.AllPairs, repo.NextErr
}
