package dummy

import "homework5/internal/pkg/repository"

type ClubsRepo struct {
	NextErr  error
	OneClub  repository.Club
	AllClubs []repository.Club
}

func (repo *ClubsRepo) Create(club repository.Club) (int, error) {
	return 1, repo.NextErr
}

func (repo *ClubsRepo) Read(id int) (repository.Club, error) {
	return repo.OneClub, repo.NextErr
}

func (repo *ClubsRepo) Update(id int, newValue repository.Club) error {
	return repo.NextErr
}

func (repo *ClubsRepo) Delete(id int) error {
	return repo.NextErr
}

func (repo *ClubsRepo) ReadAll() ([]repository.Club, error) {
	return repo.AllClubs, repo.NextErr
}
