package repository

import (
	"database/sql"
	"time"
)

type Club struct {
	Id      int    `db:"id" json:"id"`
	Name    string `db:"name" json:"name"`
	Founded int16  `db:"founded" json:"founded"`
}

type Player struct {
	Id       int           `db:"id" json:"id"`
	Name     string        `db:"name" json:"name"`
	Birthday time.Time     `db:"birthday" json:"birthday"`
	ClubId   sql.NullInt32 `db:"club_id" json:"club_id"`
}

type PlayerWithClub struct {
	PlayerId   int            `db:"player_id" json:"player_id"`
	PlayerName string         `db:"player_name" json:"player_name"`
	ClubId     sql.NullInt32  `db:"club_id" json:"club_id"`
	ClubName   sql.NullString `db:"club_name" json:"club_name"`
}
