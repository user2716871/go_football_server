-- +goose Up
-- +goose StatementBegin
create table clubs (
    id          serial constraint clubs_pk primary key,
    name        varchar(255) not null,
    founded     smallint not null
);

create table players (
     id          serial constraint players_pk primary key,
     name        varchar(255) not null,
     birthday    date not null,
     club_id     integer
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table if exists clubs;
drop table if exists players;
-- +goose StatementEnd
