package main

import (
	"fmt"
	_ "github.com/lib/pq"
	"homework5/internal/pkg/repository/postgresql"
	"homework5/internal/server"
	"log"
	"net/http"
)

const (
	inAddrAny   string = "0.0.0.0"
	defaultPort int    = 9000
)

func main() {

	s := server.NewGeneralServer(
		postgresql.NewClubsRepo(),
		postgresql.NewPlayersRepo(),
		postgresql.NewPlayersWithClubRepo())

	mux := http.NewServeMux()
	mux.HandleFunc("/", server.GetHandler(s))

	addr := fmt.Sprintf("%s:%d", inAddrAny, defaultPort)
	if err := http.ListenAndServe(addr, mux); err != nil {
		log.Fatal(err)
	}
}
